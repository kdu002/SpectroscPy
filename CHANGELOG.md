0.2.0
Added anharmonic functionality
Added possibility to skip snapshots
Fixed scaling bug in plots (now divide by number of snapshots)
Fixed outprojection for linear molecules

0.1.0

Added posibility for angular frequencies
Fixed bug with cauchy_prefactor
Fixed bug with loop index in SpectroscPy_wrapper that only came out when Raman and Hyper-Raman were run in combination, or would have been visible with multipole snapshots
average_wavenumers, all_spectroscopies_intentisites are outputs from Spectroscpy_run
get_vib_harm_freqs_and_eigvecs now takes coords, charges and masses as input instead of mol_file which makes it better compatible with DaltonProj.
OpenRSP had an update where they changed from 'NUM COMPONENTS' to 'NUM_COMPONENTS'  (or the other way around) in the rsp_tensor, now both will work fine.
Fixed bug with MDAC
Expanded tests, and added integration tests which is in practice a test of Spectroscpy_run
