# A little script to see if I can plot multiple structures in one plot nicely
# Karen Minde Dundas

from .get_spectroscopy import get_vibrational_frequencies_and_intensities, vibrationalProperty
from .plotting_module import visualize_spectrum, anharmonic_visualize_spectrum, get_plot_values
from .SpectroscPy_tools import check_spectroscopy_specifications_input, \
                               check_spectroscopy_types_input, check_command_input, \
                               check_cauchy_type, check_run_specification, get_mode_captions, \
                               get_ir_captions, get_raman_captions, get_cauchy_prefactor, \
                               get_hR_captions
from .cauchy import get_simple_averages, average_over_snapshots, get_standard_deviation
from .anharmonic import anharmonicProperty
import numpy as np
from sys import exit
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib as mpl

def remove_modes_outside_boundaries(x, y, spectral_boundaries):

    new_x = []
    new_y = []
    for i in range(len(x)):
        if (x[i] > spectral_boundaries[0] and x[i] < spectral_boundaries[1]):
            new_x.append(x[i])
            new_y.append(y[i])

    return new_x, new_y


def multi_wrapper(run_specification, spectroscopy_types, spectroscopy_specifications, cauchy_type, \
                  names, spectral_boundaries, print_level, temperature, legends, spectrum_file, raman_indices, tick_distance):

    print('This is a hack routine')

    # Anharmonic not possible atm
    anharmonic = False

    # Set captions for graphs and tables
    mode_caption, latex_mode_caption, mode_header_sting, mode_format_string = \
        get_mode_captions(spectroscopy_specifications)
    x_label = latex_mode_caption

    if ('IR' in spectroscopy_types):
        IR_caption, IR_plot_caption, latex_ir_caption, header_format_string, format_string = \
            get_ir_captions(spectroscopy_specifications)
        y_label = latex_ir_caption

    if ('Raman' in spectroscopy_types):
        raman_caption, raman_plot_caption, latex_raman_caption = \
            get_raman_captions(spectroscopy_specifications)
        y_label = latex_raman_caption

    all_y = []
    all_x = []

    # There can be more names-sets
    for i in range(len(names)):

        if ('Outproj' in run_specification[i]):
            outproj = True
        elif ('No outproj'):
            outproj = False

        temp_names = names[i]

        print('Calculation')
        if ('Single snapshot' in run_specification[i]):
            num_snapshots = 1
            range_snapshots = 1
            print('For a single snapshot')
        elif ('Multiple snapshots' in run_specification[i]):
            num_snapshots = temp_names[2] - temp_names[1] - len(temp_names[6]) + 1
            print('For multiple snapshots:', num_snapshots)
            range_snapshots = temp_names[2] - temp_names[1] + 1
        else:
            print('ERROR')

        all_harmonic_wavenumbers = []
        all_frequencies = anharmonicProperty([], [], [], [])

        if ('IR' in spectroscopy_types):
            all_ir_intensities = anharmonicProperty([], [], [], [])
        if ('Raman' in spectroscopy_types):
            all_raman_intensities = anharmonicProperty([], [], [], [])

        for j in range(range_snapshots):
            if (num_snapshots == 1):
                tensor_location = temp_names[0] + temp_names[1]
                mol_location = temp_names[0] + temp_names[2]
            else:
                snap = j + temp_names[1]

                if (snap in temp_names[6]):
                    continue

                if ('No FraME' in run_specification[i]):
                    tensor_file = temp_names[3] + '_' + temp_names[4] + '_' + \
                                  str(snap) + '.rsp_tensor'

                elif ('FraME' in run_specification[i]):
                    tensor_file = temp_names[3] + '_' + temp_names[4] + '_' + str(snap) + '_' + \
                                  temp_names[5] + '_' + str(snap) + '.rsp_tensor'

                molecule_file = temp_names[4] + '_' + str(snap) + '.mol'

                tensor_location = temp_names[0] + tensor_file
                mol_location = temp_names[0] + molecule_file

            # NOTE: further hack necessary for Raman, for now making it good for IR

            # Note that raman intensities here contains all configurations.  These are to be 
            # considered as if they were different properties, and therefore split up
            properties = get_vibrational_frequencies_and_intensities(tensor_location, mol_location, \
                                                                     spectroscopy_types, \
                                                                     spectroscopy_specifications, \
                                                                     print_level, temperature, \
                                                                     outproj, False)

            if (j == 0):
                num_modes = len(properties.frequencies.harmonic)
            else:
                if (len(properties.frequencies.harmonic) != num_modes):
                    print('\n')
                    print('Error in SpectroscPy_run')
                    print('Not all the snapshots have the same amount of vibrational modes')
                    print('Do all the rsp_tensor files belong to the same molecule?')
                    exit()

            all_frequencies.harmonic.append(properties.frequencies.harmonic)

            if ('IR' in spectroscopy_types):
                all_ir_intensities.harmonic.append(properties.ir.harmonic)

            if ('Raman' in spectroscopy_types):
                if (j == 0):
                    num_raman_configs = len(properties.raman.harmonic)
                    for k in range(num_raman_configs):
                        all_raman_intensities.harmonic.append([])

                else:
                    if (num_raman_configs != len(properties.raman.harmonic)):
                        print('NOT ALL rsp_tensors HAVE THE SAME AMOUNT OF GFF CONFIGS!! GO THROUGH YOUR')
                        print('.dal FILES AND CHECK THAT THEY ARE ALL IDENTICAL')

                # Reorganize to total array: num_raman_configs, num_snapshots, num_intensities
                for k in range(num_raman_configs):
                    all_raman_intensities.harmonic[k].append(properties.raman.harmonic[k])

        # AVERAGED VAULES PRINTING PART
        average_harmonic_frequencies = average_over_snapshots(all_frequencies.harmonic)
        average_frequencies = anharmonicProperty(average_harmonic_frequencies, [], [], [])

        if ('IR' in spectroscopy_types):
            average_harmonic_ir_intensities = average_over_snapshots(all_ir_intensities.harmonic)
            average_ir_intensities = anharmonicProperty(average_harmonic_ir_intensities, [], [], [])
        else:
            average_ir_intensities = anharmonicProperty([], [], [], [])

        if ('Raman' in spectroscopy_types):
            average_raman_harmonic = np.zeros((num_raman_configs, num_modes))

            for j in range(num_raman_configs):
                average_raman_harmonic[j] = average_over_snapshots(all_raman_intensities.harmonic[j])


            average_raman_intensities = anharmonicProperty(average_raman_harmonic, [], [], [])
        else:
            average_raman_intensities = anharmonicProperty([], [], [], [])

        if ('IR' in spectroscopy_types):
            print(('%4s' + mode_header_sting + header_format_string) %('Mode', mode_caption, \
                                                                       IR_caption))
            for j in range(num_modes):
                print(('%2d' + mode_format_string + format_string) %(j, \
                       average_frequencies.harmonic[j], average_ir_intensities.harmonic[j]))
            print('\n')
        if ('Raman' in spectroscopy_types):
            print(('%4s' + mode_header_sting + '%43s') %('Mode', mode_caption, raman_caption))
            print('%20s' %(' '), '\t'.join(['%9s %6.5E' % ('Input waven: ', val) for val in \
                                            properties.input_raman_frequencies ]))
            for j in range(num_modes):
                print(('%2d' + mode_format_string) %(j, average_frequencies.harmonic[j]), \
                       '\t'.join(['%20.10E' % average_raman_intensities.harmonic[k][j] \
                                  for k in range(num_raman_configs)]))
            print('\n')

        # Plot

        cauchy_prefactor = get_cauchy_prefactor(spectroscopy_specifications)
        if (cauchy_type == 'Discrete'):
            print('Only a stick spectrum of discrete peaks will be made')
            cauchy_type_or_value = cauchy_type
        elif (isinstance(cauchy_type, float) or isinstance(cauchy_type, int)):
            cauchy_type_or_value = float(cauchy_type)
            print('Fixed broadening factor, ', cauchy_type_or_value)
        elif (cauchy_type == 'WS'):
            cauchy_type_or_value = cauchy_type
            print('Width determined broadening factor')
        else:
            cauchy_type_or_value = 0.001*max(all_frequencies.harmonic[0])
            print('Fixed broadening factor, ', cauchy_type_or_value)

        if ('Plot IR' in run_specification[0]):
            print('IR spectrum')

            x, y, avg_w, avg_i = get_plot_values(cauchy_type, all_frequencies.harmonic, \
                                                 all_ir_intensities.harmonic)

        if ('Plot Raman' in run_specification[0]):
            print('Raman spectrum')
            x, y, avg_w, avg_i = get_plot_values(cauchy_type, all_frequencies.harmonic, \
                                                 all_raman_intensities.harmonic[raman_indices[i]])

        if (cauchy_type != 'Discrete'):
            y = np.multiply(y, cauchy_prefactor)

        x, y = remove_modes_outside_boundaries(x, y, spectral_boundaries)

        all_x.append(x)
        all_y.append(y)

    line_type = ['b', 'r', 'g', 'c', 'm', 'y']
    if (len(names) > 6):
        print('Error in analysis')
        print('Add more line types')
        exit()

    mpl.style.use('seaborn-deep')
    plt.rcParams.update({'font.size': 15})
    f, ax1 = plt.subplots(figsize=(10,5))
    for i in range(len(all_y)):
        ax1.plot(all_x[i], all_y[i], line_type[i], label=legends[i], linewidth=2.0)

    ax1.xaxis.set_major_locator(ticker.MultipleLocator(tick_distance))
    for label in ax1.xaxis.get_ticklabels()[::2]:
        label.set_visible(False)

    ax1.set_xlabel(x_label, fontsize=15)
    ax1.set_ylabel(y_label, fontsize=15)

    legend = ax1.legend(loc='upper left', fontsize=15)
    f.tight_layout()

    f.savefig(spectrum_file)








