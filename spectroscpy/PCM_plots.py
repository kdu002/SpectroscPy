# Hack module for plottin PCM Gaussian results in the same way as with LSDalton/OpenRSP results
# Karen Minde Dundas

from .plotting_module import get_plot_values
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib as mpl
import numpy as np
from math import pi, log
from .parameters import speed_of_light, plancs_constant, one_twelfth_carbon, electron_mass, \
                        bohr_to_meter, hartree_to_coulomb, hartree_to_joule
from .raman import get_exp_denominator, get_rscattering_cross_section

def extract_gauss_frequencies_and_intensities(lines):
    freq = []
    freq_indices = []
    ir = []
    ir_indices = []
    raman = []
    raman_indices = []
    for i in range(len(lines)):
        if ('Frequencies --' in lines[i]):
            freq_indices.append(i)
        if ('IR Inten    --' in lines[i]):
            ir_indices.append(i)
        if ('Raman Activ --' in lines[i]):
            raman_indices.append(i)

    for i in range(len(freq_indices)):
        temp = lines[freq_indices[i]][15:len(lines[freq_indices[i]]) - 1]
        temp = temp.split()
        for j in range(len(temp)):
            freq.append(float(temp[j]))

        temp = lines[ir_indices[i]][15:len(lines[ir_indices[i]]) - 1]
        temp = temp.split()
        for j in range(len(temp)):
            ir.append(float(temp[j]))

        temp = lines[raman_indices[i]][15:len(lines[raman_indices[i]]) - 1]
        temp = temp.split()
        for j in range(len(temp)):
            raman.append(float(temp[j]))

    return freq, ir, raman


def convert_raman(raman_A4amu, wn_cm):

    raman_combo_SI = \
        np.multiply(raman_A4amu, electron_mass/(one_twelfth_carbon*(bohr_to_meter*10**10)**4))
    raman_combo_SI = np.multiply(raman_combo_SI, hartree_to_coulomb**4*bohr_to_meter**2/ \
                                                 (hartree_to_joule**2*electron_mass))

    wn_m = np.multiply(wn_cm, 100)

    exp_denom = get_exp_denominator(wn_m, 298)
    prefactor = plancs_constant/(8.0*pi**2*speed_of_light)

    # I think Gaussian calculates in the static limit
    intensities = get_rscattering_cross_section(exp_denom, 0.0, wn_m, raman_combo_SI)
    intensities = np.multiply(intensities, prefactor)
    intensities = np.divide(intensities, wn_m)

    return intensities


def reorder_values(values):

    reordered = []
    for i in range(len(values) - 1, -1, -1):
        reordered.append(values[i])

    reordered = np.array(reordered)
    return reordered


def PCM_plotting(gaussian_outputs, cauchy_type, legends, spectrum_file, spectral_boundaries, ticker_distance):

    all_ir = []
    all_raman = []
    all_ir_x = []
    all_raman_x = []

    cauchy_prefactor = 0.01/(2*pi*speed_of_light)

    for i in range(len(gaussian_outputs)):

        file = open(gaussian_outputs[i])
        lines = file.readlines()
        file.close()

        # Values in 1/cm, km/mol and A**4/amu (I am uncertain of combo-rule)
        freq, ir, raman = extract_gauss_frequencies_and_intensities(lines)

        freq = reorder_values(freq)
        ir = reorder_values(ir)
        raman = reorder_values(raman)

        print('PCM VALUES', legends[i])
        print('IR')
        print(('%4s' + '%18s' + '%18s') %('Mode', 'Wavenumber 1/cm', 'IR km/mol'))
        for j in range(len(freq)):
            print(('%2d' + '%19.6f' + '%17.6f') %(j + 1, freq[j], ir[j]))

        print('Raman')
        print(('%4s' + '%18s' + '%18s') %('Mode', 'Wavenumber 1/cm', 'Raman A4/amu'))
        for j in range(len(freq)):
            print(('%2d' + '%19.6f' + '%17.6f') %(j + 1, freq[j], raman[j]))


        # Convert to sigma
        raman = convert_raman(raman, freq)
        
        print('print for comparison')
        print(freq)
        print(ir)
        print(np.multiply(raman, 1.0e60))

        # Latex printing
        for j in range(len(freq)):
            print(('%2d' + '%3s' + '%6d' +'%3s' + '%11.3f' + '%3s' + '%11.3f' + '%3s' ) %(j + 1, '&', freq[j], '&', ir[j], '&', np.multiply(raman[j], 1.0e60), '\\\\'))

        # Convert to m^2/mol for plotting
        ir = np.multiply(ir, 2000*speed_of_light*pi/log(10))

        x, y, avg_w, avg_i = get_plot_values(cauchy_type, [freq], [ir])

        y = np.multiply(y, cauchy_prefactor)

        all_ir.append(y)
        all_ir_x.append(x)

        x, y, avg_w, avg_i = get_plot_values(cauchy_type, [freq], [raman])

        y = np.multiply(y, cauchy_prefactor)

        all_raman.append(y)
        all_raman_x.append(x)

    line_type = ['b', 'r', 'g', 'c', 'm', 'y']
    if (len(gaussian_outputs) > 6):
        print('Error in analysis')
        print('Add more line types')
        exit()

    x_label = r'$\bar{\nu} \; / \; (\mathrm{cm}^{-1})$'
    y_label = r'$\varepsilon \; / \; (\mathrm{m}^2\cdot \mathrm{mol}^{-1})$'
    mpl.style.use('seaborn-deep')
    plt.rcParams.update({'font.size': 15})
    f, ax1 = plt.subplots(figsize=(10,5))
    for i in range(len(all_ir)):
        ax1.plot(all_ir_x[i], all_ir[i], line_type[i], label=legends[i], linewidth=2.0)

    ax1.xaxis.set_major_locator(ticker.MultipleLocator(ticker_distance))
    for label in ax1.xaxis.get_ticklabels()[::2]:
        label.set_visible(False)

    ax1.set_xlabel(x_label, fontsize=15)
    ax1.set_ylabel(y_label, fontsize=15)

    legend = ax1.legend(loc='upper left', fontsize=15)
    f.tight_layout()


    if (spectral_boundaries != 'Keep all'):
        plt.xlim(spectral_boundaries[0], spectral_boundaries[1])
    plt.show()

    f.savefig(spectrum_file[0])

    x_label = r'$\bar{\nu} \; / \; (\mathrm{cm}^{-1})$'
    y_label = r'$\sigma^{\prime} \; / \; (\mathrm{C}^{4}\mathrm{s}^3\mathrm{J}^{-1}\mathrm{m}^{-2}\mathrm{kg}^{-1})$'
    mpl.style.use('seaborn-deep')
    plt.rcParams.update({'font.size': 15})
    f, ax1 = plt.subplots(figsize=(10,5))
    for i in range(len(all_raman)):
        ax1.plot(all_raman_x[i], all_raman[i], line_type[i], label=legends[i], linewidth=2.0)

    ax1.xaxis.set_major_locator(ticker.MultipleLocator(ticker_distance))
    for label in ax1.xaxis.get_ticklabels()[::2]:
        label.set_visible(False)

    ax1.set_xlabel(x_label, fontsize=15)
    ax1.set_ylabel(y_label, fontsize=15)

    legend = ax1.legend(loc='upper left', fontsize=15)
    f.tight_layout()

    if (spectral_boundaries != 'Keep all'):
        plt.xlim(spectral_boundaries[0], spectral_boundaries[1])
    plt.show()

    f.savefig(spectrum_file[1])
