# Use the already optimized input structures
from .vib_analysis import read_mol
from .parameters import bohr_to_meter
from math import sqrt, acos
import numpy as np


def get_distance(coords1, coords2):

    length = 0.0
    for i in range(3):
        length = length + (coords1[i] - coords2[i])**2

    length = sqrt(length)

    return length


# Specific to Acetone
def extract_gaussian_coordinates(folder, inp):

    file = open(folder + inp)
    lines = file.readlines()
    file.close()

    start_line = 0
    for i in range(len(lines)):
        if (lines[i] == '0 1\n'):
            start_line = i + 1
            break

    coords = np.zeros((10, 3))
    for i in range(10):
        temp = lines[i + start_line][2:38]
        temp = temp.split()
        for j in range(3):
            coords[i][j] = float(temp[j])

    return coords


def find_Gaussian_bond_length(folder, mol):

    coords = extract_gaussian_coordinates(folder, mol)

    lengths = np.zeros((len(coords), len(coords)))
    for i in range(len(coords)):
        for j in range(len(coords)):
            lengths[i][j] = get_distance(coords[i], coords[j])

    return lengths


def find_LSDalton_bond_length(folder, mol):

    coords, charges, masses = read_mol(folder + mol)
    A_coords = np.multiply(coords, bohr_to_meter*10**10)

    lengths = np.zeros((len(coords), len(coords)))
    for i in range(len(coords)):
        for j in range(len(coords)):
            lengths[i][j] = get_distance(A_coords[i], A_coords[j])

    return lengths


def get_bond_angle(a, b, c):

    angle = (a**2 + b**2 - c**2)/(2*a*b)
    angle = acos(angle)

    radians_to_degrees = 57.2958

    angle = angle*radians_to_degrees

    return angle


# Specific to acetone
# Expects the following ordering: 3C, 1O, 6H
def get_bond_lengths(ls_names, multiple_snapshots, g_folder, g_inp):

    if (multiple_snapshots):
        num_snapshots = ls_names[2] - ls_names[1] - len(ls_names[6]) + 1
        range_snapshots = ls_names[2] - ls_names[1] + 1
    else:
        num_snapshots = 1
        range_snapshots = 1

    ls_folder = ls_names[0]
    ls_lengths = 0.0
    for j in range(range_snapshots):
        if (num_snapshots == 1):
            ls_mol = ls_names[2]
        else:
            snap = j + ls_names[1]
            if (snap in ls_names[6]):
                continue
            ls_mol = ls_names[4] + '_' + str(snap) + '.mol'

        ls_lengths = ls_lengths + find_LSDalton_bond_length(ls_folder, ls_mol)

    ls_lengths = ls_lengths/num_snapshots

    g_lengths = find_Gaussian_bond_length(g_folder, g_inp)

    print('***************************')
    print('C-C bond lengths')
    print('%7s' % ' ', 'LSDalton', 'Gaussian')
    print('%6s' % 'C1-C2', '%8.4f' % ls_lengths[0][1], '%8.4f' % g_lengths[0][1])
    print('%6s' % 'C1-C3', '%8.4f' % ls_lengths[0][2], '%8.4f' % g_lengths[0][2])
    #print('%6s' % 'C2-C3', '%8.4f' % ls_lengths[1][2], '%8.4f' % g_lengths[1][2])
    print('\n')

    print('***************************')
    print('C-O bond lengths')
    print('%7s' % ' ', 'LSDalton', 'Gaussian')
    print('%6s' % 'C1-O', '%8.4f' % ls_lengths[0][3], '%8.4f' % g_lengths[0][3])
    #print('%6s' % 'C2-O', '%8.4f' % ls_lengths[1][3], '%8.4f' % g_lengths[1][3])
    #print('%6s' % 'C3-O', '%8.4f' % ls_lengths[2][3], '%8.4f' % g_lengths[2][3])
    print('\n')

    print('***************************')
    print('C-H bond lengths')
    print('%7s' % ' ', 'LSDalton', 'Gaussian')
    #print('%6s' % 'C1-H1', '%8.4f' % ls_lengths[0][4], '%8.4f' % g_lengths[0][4])
    #print('%6s' % 'C1-H2', '%8.4f' % ls_lengths[0][5], '%8.4f' % g_lengths[0][5])
    #print('%6s' % 'C1-H3', '%8.4f' % ls_lengths[0][6], '%8.4f' % g_lengths[0][6])
    #print('%6s' % 'C1-H4', '%8.4f' % ls_lengths[0][7], '%8.4f' % g_lengths[0][7])
    #print('%6s' % 'C1-H5', '%8.4f' % ls_lengths[0][8], '%8.4f' % g_lengths[0][8])
    #print('%6s' % 'C1-H6', '%8.4f' % ls_lengths[0][9], '%8.4f' % g_lengths[0][9])
    print('%6s' % 'C2-H1', '%8.4f' % ls_lengths[1][4], '%8.4f' % g_lengths[1][4])
    print('%6s' % 'C2-H2', '%8.4f' % ls_lengths[1][5], '%8.4f' % g_lengths[1][5])
    print('%6s' % 'C2-H3', '%8.4f' % ls_lengths[1][6], '%8.4f' % g_lengths[1][6])
    #print('%6s' % 'C2-H4', '%8.4f' % ls_lengths[1][7], '%8.4f' % g_lengths[1][7])
    #print('%6s' % 'C2-H5', '%8.4f' % ls_lengths[1][8], '%8.4f' % g_lengths[1][8])
    #print('%6s' % 'C2-H6', '%8.4f' % ls_lengths[1][9], '%8.4f' % g_lengths[1][9])
    #print('%6s' % 'C3-H1', '%8.4f' % ls_lengths[2][4], '%8.4f' % g_lengths[2][4])
    #print('%6s' % 'C3-H2', '%8.4f' % ls_lengths[2][5], '%8.4f' % g_lengths[2][5])
    #print('%6s' % 'C3-H3', '%8.4f' % ls_lengths[2][6], '%8.4f' % g_lengths[2][6])
    print('%6s' % 'C3-H4', '%8.4f' % ls_lengths[2][7], '%8.4f' % g_lengths[2][7])
    print('%6s' % 'C3-H5', '%8.4f' % ls_lengths[2][8], '%8.4f' % g_lengths[2][8])
    print('%6s' % 'C3-H6', '%8.4f' % ls_lengths[2][9], '%8.4f' % g_lengths[2][9])
    print('\n')

    print('***************************')
    print('C-C-H bond angles')
    print('%7s' % ' ', 'LSDalton', 'Gaussian')
    print('%9s' % 'H1-C2-C1', '%8.4f' % get_bond_angle(ls_lengths[1][4], ls_lengths[0][1], ls_lengths[0][4]), '%8.4f' % get_bond_angle(g_lengths[1][4], g_lengths[0][1], g_lengths[0][4]))
    print('%9s' % 'H2-C2-C1', '%8.4f' % get_bond_angle(ls_lengths[1][5], ls_lengths[0][1], ls_lengths[0][5]), '%8.4f' % get_bond_angle(g_lengths[1][5], g_lengths[0][1], g_lengths[0][5]))
    print('%9s' % 'H3-C2-C1', '%8.4f' % get_bond_angle(ls_lengths[1][6], ls_lengths[0][1], ls_lengths[0][6]), '%8.4f' % get_bond_angle(g_lengths[1][6], g_lengths[0][1], g_lengths[0][6]))
    print('%9s' % 'H4-C3-C1', '%8.4f' % get_bond_angle(ls_lengths[2][7], ls_lengths[2][0], ls_lengths[0][7]), '%8.4f' % get_bond_angle(g_lengths[2][7], g_lengths[2][0], g_lengths[0][7]))
    print('%9s' % 'H5-C3-C1', '%8.4f' % get_bond_angle(ls_lengths[2][8], ls_lengths[2][0], ls_lengths[0][8]), '%8.4f' % get_bond_angle(g_lengths[2][8], g_lengths[2][0], g_lengths[0][8]))
    print('%9s' % 'H4-C3-C1', '%8.4f' % get_bond_angle(ls_lengths[2][9], ls_lengths[2][0], ls_lengths[0][9]), '%8.4f' % get_bond_angle(g_lengths[2][9], g_lengths[2][0], g_lengths[0][9]))
    print('\n')

    return ls_lengths, g_lengths


def rmsd(value, ref_value):
    rmsd = 0.0
    for i in range(len(value)):
        rmsd = rmsd + (value[i] - ref_value[i])**2

    rmsd = rmsd/(len(value))
    rmsd = sqrt(rmsd)

    return rmsd


# Specific to acetone
def bond_lengths_multiple_systems(labels, ls_names, multiple_snapshots, g_folders, g_inps):

    ls_lengths = []
    g_lengths = []

    for i in range(len(labels)):
        print('********************************************************************************')
        print('Bond lengths for ', labels[i])

        ls_temp, g_temp = get_bond_lengths(ls_names[i], multiple_snapshots[i], g_folders[i], g_inps[i])

        ls_lengths.append(ls_temp)
        g_lengths.append(g_temp)

    print('LATEX table')
    print('%6s' % 'Mode', '\t'.join(['%18s' % labels[i] for i in range(len(labels))]))
    print('%6s' % ' ', '\t'.join(['%20s' % 'LSDalton  Gaussian' for i in range(len(labels))]))

    print('***************************')
    print('C-C bond lengths')
    print('%6s' % 'C1-C2', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_lengths[i][0][1], '&',  g_lengths[i][0][1], '&') for i in range(len(labels))]))
    print('%6s' % 'C1-C3', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_lengths[i][0][2], '&',  g_lengths[i][0][2], '&') for i in range(len(labels))]))

    ls_c_c_average = np.zeros((len(labels)))
    g_c_c_average = np.zeros((len(labels)))
    for i in range(len(labels)):
        ls_c_c_average[i] = (ls_lengths[i][0][1] + ls_lengths[i][0][2])/2.0
        g_c_c_average[i] = (g_lengths[i][0][1] + g_lengths[i][0][2])/2.0

    print('%6s' % 'Avg.', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_c_c_average[i], '&',  g_c_c_average[i], '&') for i in range(len(labels))]))

    ls_cc_rmsd = np.zeros((len(labels)))
    g_cc_rmsd = np.zeros((len(labels)))
    for i in range(len(labels)):
        ls_cc_rmsd[i] = rmsd(ls_lengths[i][0][0:2], ls_lengths[0][0][0:2])
        g_cc_rmsd[i] = rmsd(g_lengths[i][0][0:2], g_lengths[0][0][0:2])

    print('%6s' % 'RMSD', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_cc_rmsd[i], '&',  g_cc_rmsd[i], '&') for i in range(len(labels))]))

    print('\n')

    print('***************************')
    print('C-O bond lengths')
    print('%6s' % 'C1-O', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_lengths[i][0][3], '&',  g_lengths[i][0][3], '&') for i in range(len(labels))]))

    ls_co_rmsd = np.zeros((len(labels)))
    g_co_rmsd = np.zeros((len(labels)))
    for i in range(len(labels)):
        ls_co_rmsd[i] = abs(ls_lengths[i][0][3] - ls_lengths[0][0][3])
        g_co_rmsd[i] = abs(g_lengths[i][0][3] - g_lengths[0][0][3])
    
    print('%6s' % 'Diff.', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_co_rmsd[i], '&',  g_co_rmsd[i], '&') for i in range(len(labels))]))

    print('\n')

    print('***************************')
    print('C-H bond lengths')
    print('%6s' % 'C2-H1', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_lengths[i][1][4], '&',  g_lengths[i][1][4], '&') for i in range(len(labels))]))
    print('%6s' % 'C2-H2', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_lengths[i][1][5], '&',  g_lengths[i][1][5], '&') for i in range(len(labels))]))
    print('%6s' % 'C2-H3', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_lengths[i][1][6], '&',  g_lengths[i][1][6], '&') for i in range(len(labels))]))
    print('%6s' % 'C3-H4', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_lengths[i][2][7], '&',  g_lengths[i][2][7], '&') for i in range(len(labels))]))
    print('%6s' % 'C3-H5', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_lengths[i][2][8], '&',  g_lengths[i][2][8], '&') for i in range(len(labels))]))
    print('%6s' % 'C3-H6', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_lengths[i][2][9], '&',  g_lengths[i][2][9], '&') for i in range(len(labels))]))

    ls_ch_average = np.zeros((len(labels)))
    g_ch_average = np.zeros((len(labels)))
    for i in range(len(labels)):
        ls_ch_average[i] = (ls_lengths[i][1][4] + ls_lengths[i][1][5] + ls_lengths[i][1][6] + ls_lengths[i][2][7] + ls_lengths[i][2][8] + ls_lengths[i][2][9])/6.0
        g_ch_average[i] = (g_lengths[i][1][4] + g_lengths[i][1][5] + g_lengths[i][1][6] + g_lengths[i][2][7] + g_lengths[i][2][8] + g_lengths[i][2][9])/6.0

    print('%6s' % 'Avg.', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_ch_average[i], '&',  g_ch_average[i], '&') for i in range(len(labels))]))

    ls_ch = [[], [], [], []]
    g_ch = [[], [], [], []]
    for i in range(len(labels)):
        ls_ch[i].append(ls_lengths[i][1][4])
        ls_ch[i].append(ls_lengths[i][1][5])
        ls_ch[i].append(ls_lengths[i][1][6])
        ls_ch[i].append(ls_lengths[i][2][7])
        ls_ch[i].append(ls_lengths[i][2][8])
        ls_ch[i].append(ls_lengths[i][2][9])
        g_ch[i].append(g_lengths[i][1][4])
        g_ch[i].append(g_lengths[i][1][5])
        g_ch[i].append(g_lengths[i][1][6])
        g_ch[i].append(g_lengths[i][2][7])
        g_ch[i].append(g_lengths[i][2][8])
        g_ch[i].append(g_lengths[i][2][9])

    ls_ch_rmsd = np.zeros((len(labels)))
    g_ch_rmsd = np.zeros((len(labels)))
    for i in range(len(labels)):
        ls_ch_rmsd[i] = rmsd(ls_ch[i], ls_ch[0])
        g_ch_rmsd[i] = rmsd(g_ch[i], g_ch[0])

    print('%6s' % 'RMSD', '%1s' % '&', '\t'.join([ '%8.4f %3s %8.4f %3s' % (ls_ch_rmsd[i], '&',  g_ch_rmsd[i], '&') for i in range(len(labels))]))
    
    
    print('\n')

