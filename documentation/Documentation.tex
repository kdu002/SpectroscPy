\documentclass[12pt,a4paper]{article} %book
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{bm}
\usepackage{booktabs}
\usepackage[super]{natbib}
\usepackage[margin=1in]{geometry}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{hyperref}
\usepackage{xcolor}
\def\msquare{\mathord{\scalerel*{\Box}{gX}}}
\numberwithin{equation}{section}
\begin{document}
	\author{Karen Oda Hjorth Minde Dundas}
	\title{Documentation SpectroscPy}
	\maketitle
	
SpectroscPy is a Python scriptpackage designed to postprocess outputs from OpenRSP/LSDalton response property calculations and determine the desired spectroscopic properties.  As of August 2019, it works for vibrational properties, specifically IR and Raman, but can be easily extended to other types of spectroscopies.  It should also be a small task to link it to other program packages.

This document is a user guide, and should get you ready to use the package.

You run the code by calling \verb|SpectroscPy_run| which is inside the file \verb|SpectroscPy_wrapper.py|.  An example is as follows

\begin{verbatim}
	from SpectroscPy_wrapper import SpectroscPy_run
	run_specification = ['Multiple snapshots', 'No FraME', 'Plot IR', 'Outproj']
	spectroscopy_type = ['IR']
	sectroscopy_specification = ['Vib modes: 1/m', 'IR: MDAC, m**2/(s*mol)']
	cauchy_type = 100.0
	names = ['test_w_mult_snapshots/', 0, 5, 'hf', 'optimized', 'No FraME', [2]]
	harmonic_frequency_limits = 'Keep all'
	spectral_boundaries = 'Keep all'
	T = 298
	print_level = 0
	SpectroscPy_run(run_specification, spectroscopy_type, spectroscopy_specification, 
	cauchy_type, names, harmonic_frequency_limits, spectral_boundaries, print_level, T)
\end{verbatim}
%
This is a calculation for multipole snapshots without FraME, to get both table values and a plotted spectrum of the molar decadic attenuated IR coefficients in $\text{m}^2/(\text{s}\cdot\text{mol})$ units.  The broadening factor of the plot is given, and is 100.0.  I will now go through the various different options one at a time and explain them.

\section{Preparations}
Prior to SpectroscPy calculation, you have to run an OpenRSP/LSDalton calculation to obtain the tensors needed for the desired spectroscopy.  For IR this is the molecular Hessian and dipole gradient, while for Raman it is the moelcular Hessian and the polarizability gradient.  This gives a file of the type \verb|.rps_tensor| which we need.

\section{\texttt{run\_specification}}
Contains four different categories, how many snapshots, is it with or without PE, what should be plotted and whether the rotational and translational modes should be projected out or not.

\subsection*{Snapshots}
You have to specify if this is a calculation for a single structure, or if it is for a set of snapshots from an MD simulation.  The first is typically the case if you have a vacuum structure, and the other if you have one with environment where you sample the configurations.  The options (you must choose one of them) are \verb|'Single snapshot'| or \verb|'Multiple snapshots'|.

\subsection*{With or without PE}
Choose whether you would like to use the PE method for the environment.  The options (you must choose one of them) are \verb|'FraME'| or \verb|'No FraME'|.

\subsection*{What to plot}
When you calculate values for IR and/or Raman spectroscopy, you can choose whether you want to plot these or not.  If you choose not to, the average values will still be printed.  You can also choose how many lines you want in each plot.  If you choose one of the options with more than one line per plot, be aware that all the lines will be scaled to max 1.0.  Choose as many as you wish from the list below, or none.  

\begin{itemize}
	\item \verb|'Plot IR'|
	\item \verb|'Plot Raman separately'|
	\item \verb|'Plot Raman together'|
	\item \verb|'Plot IR and individual Raman together'|
	\item \verb|'Plot IR and all Raman together'|
	\item \verb|'Plot all Hyper-Raman separately'|
	\item \verb|'Plot Hyper-Raman VV and HV separately'|
	\item \verb|'Plot all Hyper-Raman together'|
\end{itemize}

\subsection*{Outprojection or not}
For vacuum calculations it is common to project out rotational and translational modes\cite{ofstad2014vibrational} in order to isolate the vibrational modes.  The effect of this in a constraining environment is however not wellknown, and an option to not to perform this outprojection is therefore given.  Specify one of the following (you must choose one) \verb|'Outproj'| or \verb|'No outproj'|.

\subsection*{Symbolic or written axis labels}
The default label-type on the plot axes is a written one, but if you wish, you can choose a symbolic one.  Do that by specifying \verb|'Symbolic labels'|.  This will for instance change \verb|IR MDAC L/(cm*mol)| to $\varepsilon : \mathrm{L}\cdot \mathrm{cm}^{-1}\mathrm{mol}^{-1}$.

\subsection*{Anharmonic}
Anharmonic corrections can be calculated, both for frequencies and intensities.  Various levels of theory are available, for intensities, up to DVPT2, and for frequencies up to GVPT2.  The options are made with the view of being comparable to Gaussian, and are

\begin{itemize}
	\item Nothing: harmonic
	\item \verb|'Harmonic'|: harmonic
	\item \verb|'Anharmonic'|: GVPT2 frequencies and DVPT2 intensities, all with 1-1 resonance checks (default).
	\item \verb|'Anharmonic: Freq DVPT2, Int VPT2''|: DVPT2 frequencies and VPT2 intensities.
	\item \verb|'Anharmonic: DVPT2'|: DVPT2 frequencies and intensities.
	\item \verb|'Anharmonic Freq: DVPT2'|: DVPT2 frequencies and intensities.
	\item \verb|'Anharmonic: DVPT2, w/ 1-1 checks'|: DVPT2 frequencies and intensities, with 1-1 resonance checks.
	\item \verb|'Anharmonic: Freq GVPT2, Int DVPT2'|: GVPT2 frequencies and DVPT2 intensities
	\item \verb|'Anharmonic: Freq GVPT2, Int DVPT2, w/ 1-1 checks'|: GVPT2 frequencies and DVPT2 intensities, with 1-1 resonance checks (same as \verb|'Anharmonic'|).
\end{itemize}

The fermi resonance checks are based upon 

\begin{equation}
	|\omega_i - \omega_j - \omega_k| \leq 200 \mathrm{cm}^{-1}
\end{equation}

\begin{equation}
	\Delta \geq 1 \mathrm{cm}^{-1}
\end{equation}
%
where $\Delta$ is given by the Martin parameters\cite{martin1995anharmonic}.  In order to identify a Fermi resonance, both conditions must be satisfied.  The 1-1 checks are based on 

\begin{equation}
	|\omega_i - \omega_j| \leq 100 \mathrm{cm}^{-1}
\end{equation}

\begin{equation}
	|K| \geq 1 \mathrm{cm}^{-1}
\end{equation}

\begin{equation}
\frac{|K|}{(\omega_i - \omega_j)^2} \geq  1 \mathrm{cm}^{-1}
\end{equation}
%
where $K$ is the coupling of the term; $k_{ijkl}$ for quartic terms, $k_{ijk}\cdot k_{lmn}$ for cubic terms and the coefficient sum for the Coriolis terms.  For a 1-1 resonance to be identified, the first condition must be true, and at least one of the two others.

\section{\texttt{spectroscopy\_type}}
Here you choose which spectroscopic values you wish to calculate.  Choose at least (but more if you want) among \verb|'IR'|, \verb|'Raman'| and \verb|'Hyper-Raman'|.  You must choose at least one.

\section{\texttt{spectroscopy\_specification}}
In the literature and in many codes there is a high degree of ambiguity when it comes to what is actually meant by IR 'intensities'.  In many cases it is actually the molar attenuated coefficient, or parts of it, that is reported.  To remove any uncertainty, when you have \verb|'IR'| in \verb|spectroscopy_type|, you have to specify one of the following through \verb|spectroscopy_specification|

\begin{itemize}
	\item \verb|'IR: SSDG, a.u.'|: Infrared SSDG (summed and squared dipole gradients) in a.u
	\item  \verb|'IR: SSDG, C**2/kg'|: Infrared SSDG (summed and squared dipole gradients) in $\text{C}^2/\text{kg}$
	\item \verb|'IR: SSDG, D2A2/amu'|: Infrared SSDG (summed and squared dipole gradients) in $\text{(D/A)}^2/\text{amu}$.  A is here for angstrom.
	\item \verb|'IR: MDAC, m**2/(s*mol)'|: Infrared MDAC (Molar decadic attenuated coefficient) in $\text{m}^2/\text{mol}$, or w/o lineshape in $\text{m}^2/(\text{s}\cdot\text{mol})$
	\item \verb|'IR: MDAC, L/(cm*s*mol)'|: Infrared MDAC (Molar decadic attenuated coefficient in L/(cm*mol), or w/o lineshape in L/(cm*s*mol)
	\item \verb|'IR: NIMAC, m/mol'|: Infrared NIMAC (Naperian integrated molar absorption coefficient) in m/mol
	\item \verb|'IR: NIMAC, km/mol'|: Infrared NIMAC (Naperian integrated molar absorption coefficient) in km/mol (Default in Dalton)
\end{itemize}

The definitions of these values will be described in more detail in my thesis or somewhere else, but if you would like to know them before it's finished, contact me.

Raman is by in the litterature reported in arbitrary units, but it is useful to make distinctions on what these are based on.  In SpectroscPy there are no arbitrary units, everything is based on absolute values.  There is also a choice regarding the combinination of the polarizability gradient terms $a_i^2$ and $b_i^2$. You therefore need to specify one of the following through \verb|spectroscopy_specification|

\begin{itemize}
	\item \verb|'Raman: CPG 45+4, a.u.'|: Combined polarizability gradients for combination $(45a_i^2 + 4b_i^2)$ in a.u.
	\item \verb|'Raman: CPG 45+7, a.u.'|: Combined polarizability gradients for combination $(45a_i^2 + 7b_i^2)$ in a.u.
	\item \verb|'Raman: PCPG 45+4, Å^4/amu'|: Pseudo combined polarizability gradients for combination $(45a_i^2 + 4b_i^2)$ in $\text{Å}^4\cdot \text{amu}^{-1}$ (Default in Dalton)
	\item \verb|'Raman: PCPG 45+7, Å^4/amu'|: Pseudo combined polarizability gradients for combination $(45a_i^2 + 7b_i^2)$ in $\text{Å}^4\cdot \text{amu}^{-1}$
	\item \verb|'Raman: SCS 45+4, SI units'|: Absolute differential scattering cross section for combination $(45a_i^2 + 4b_i^2)$ in SI units
	\item \verb|'Raman: SCS 45+7, SI units'|: Absolute differential scattering cross section for combination $(45a_i^2 + 7b_i^2)$ in SI units
\end{itemize}

For hyper-Raman the literature is less diverse, and the only options (one must be chosen) are therefore

\begin{itemize}
	\item \verb|'Hyper-Raman: SI complete'|: Straight forward calculation in SI units
	\item \verb|'Hyper-Raman: Relative'|: Since hyper-Raman intensities often are of very small magnitudes, it is sometimes convenient to report them relative to each other, $I^{\mathrm{rel}}_a = I_a /(I_a + I_b + \cdots)$.  However, take care with this option if outprojection is not chosen.
\end{itemize}

For questions, the same holds as for IR, contact me or check my thesis.

In many quantum chemistry code, many different quantities are reported as "intensities".  Specify one of the following through \verb|spectroscopy_specification| 

\begin{itemize}
	\item \verb|'Vib modes: 1/m'|: Wavenumbers in $\text{m}^{-1}$.
	\item \verb|'Vib modes: 1/cm'|: Wavenumbers in $\text{cm}^{-1}$.
	\item \verb|'Vib modes: 1/s'|: Frequencies in $\text{s}^{-1}$.
	\item \verb|'Vib modes: ang. 1/s'|: Angular frequencies in $\text{s}^{-1}$.
	\item \verb|'Vib modes: Eh'|: Vibrational energies in hartree.
\end{itemize} 

\section{\texttt{cauchy\_type}}
When plotting a spectrum, we need to somehow determine a broadening factor, unless we wish only a stick spectrum.  The latter can be fine for a single snapshot, but definitely is not a good representation for multiple snapshots.  SpectroscPy makes a lineshape based on the Cauchy distribution, more on which can be found in my thesis.  For this distribution, a parameter $\gamma$ needs to be determined.  We can do this either by specifying or collecting a default fixed $\gamma$ value for the whole spectrum, or determining a $\gamma$ value for each different vibrational mode based on the distribution of the snapshots for that specific peak.  In this version you have to choose one of the following options for \verb|cauchy_type|

\begin{itemize}
	\item Any float or integer, which will be used as the $\gamma$ parameter.  All snapshots will be used in the plotting
	\item \verb|''| or \verb|'GS'| will both give a default $\gamma$ parameter, which is based on the maximum wavenumber value.  GS stands for given gamma, all snapshots.  All snapshots will be used in the plotting
	\item \verb|'WS'| creates a $\gamma$ parameter based on the distribution of the snapshots for each given peak.  All snapshots will be used in the plotting
    \item \verb|'Discrete'| creates a stick spectrum with no broadening.
\end{itemize}

\section{\texttt{names}}
Names of files to be used as input.  Here are two different setups, one for a single snapshot and one for multiple.  It is important that the ordering is as specified as below, which is a comment from \verb|SpectroscPy_run|

\begin{verbatim}
	# names contains the names and locations etc for the files.  
	# Two different setups, either for single or multiple snapshots
	# A) Single snapshot
	#    0) file location: folder where files are stored.  Must have backslash after
	#    1) rsp_tensor file
	#    2) mol file
	# B) Multiple snapshots
	#    0) file location: folder where files are stored
	#    1) start_snapshot: start number
	#    2) end_snapshot: end number
	#    3) dal name base
	#    4) mol name base
	#    5) json name base.  If FraME not used, write 'No FraME'
	#    6) list of any snapshots you wish to ship, f.eks [4, 48]
\end{verbatim}

\section{Other inputs}
\verb|harmonic_frequency_limits| specify cut-off of harmonic frequencies.  When running calculations with solvent, some lower-frequency modes might be seen as contaminated by translation and rotation.  These should therefore be removed so as not to confuse your results and to not enter into seemingly higher-frequency overtones and combination bands.  If you wish to keep all modes, choose \verb|'Keep all'|, choose minimum and maximum values as in \verb|harmonic_frequency_limits = [min_x, max_x]|.  In the latter case, make sure that you choose a \verb|max_x| value higher than you highest frequency when performing anharmonic calculations.  Otherwise you might up severely changing the anharmonic corrected results, also for modes that you have not chosen to remove.

\verb|spectral_boundaries| specify the boundaries of the x-axis.  If you wish to plot the whole spectrum, choose \verb|'Keep all'|, choose minimum and maximum values as in \\ \verb|spectral_boundaries = [min_x, max_x]|.

\verb|T| is the temperature in Kelvin, and is a mandatory argument in \verb|SpectroscPy_run| even though it is only used for Raman.  If you are running IR, just put T to some random value.

\verb|print_level| is a number speicifying the level of printing.  0 is the lowest possible value.

\section{Citations/Aknowledgements}
SpectroscPy is a program package developed by and containing contributions from

\begin{itemize}
	\item Karen Dundas
	\item Magnus Ringholm
	\item Yann Cornaton
	\item Benedicte Ofstad
\end{itemize}
%
However, we would also like to thank Orian Louant and Per-Olof Åstrand who have been present at each their stage. John Kendrick should be mentioned in relation to the IR units, and the mass dictionary used in \verb|vib_analysis.py| is taken from Gohlke's \verb|elements.py|\cite{gohlke}.  Also Jógvan Magnus Haugaard Olsen for discussion, technical help and other guidance.  

It should also be noted that the functionality in \verb|transform_nc_to_nm.py| and \\
\verb|vib_analysis.py| to various extents replicates or was written on the basis of (or inspired by) corresponding functionality in the Dalton quantum chemistry program and unreleased code associated with Dalton/OpenRSP. In the released Dalton program, the source code files containing this functionality are DALTON/abacus/abavib.F and DALTON/abacus/hergp.F. An overview of the routines in these two scripts for which this applies and the routines in Dalton from which the functionality was replicated, based upon or inspired by, can be found in Table \ref{Daltonleg}.

\small{
\begin{table}[h!]
	\centering
	\caption{Origin/inspiration of routines}
	\label{Daltonleg}
	\begin{tabular}{c c}
		Routine in \verb|transform_nc_to_nm.py|&     Corresponding routine(s)    \\
		\hline
        \verb|transform_cartesian_to_normal_one_rank| & VIBV3 in DALTON/abacus/abavib.F \\
         &  (and unreleased OpenRSP/Dalton functionality)\\
         \verb|transform_cartesian_to_normal| & VIBV3 in DALTON/abacus/abavib.F \\
          &   (and unreleased OpenRSP/Dalton functionality) \\
         \hline
         Routine in \verb|vib_analysis.py| & Corresponding routine(s) \\
         \hline
         \verb|project_out_transl_and_rot| & VIBHES in DALTON/abacus/abavib.F \\
         \verb|get_vib_harm_freqs_and_eigvecs| & VIBNOR and VIBANA, possibly also VIBCTL,\\
         &  ISOMOL (all former four in \\
          & DALTON/abacus/abavib.F), \\
          & VIBMAS in DALTON/abacus/hergp.F
	\end{tabular}
\end{table}
}


Dalton is released under a GNU Lesser General Public License v2.1, which can be found at
https://gitlab.com/dalton/dalton/blob/master/LICENSE.  The full sourcecode of Dalton can be found at https://gitlab.com/dalton/dalton.

\section{Brief history of the code}
SpectroscPy grew out of a need to collect and systematize many unpublished scripts that had been used by the authors for various projects over the years.  Ofstad's master thesis from 2014\cite{ofstad2014vibrational} is one example, as is Cornaton, Ringholm, Louant, Ruud and others' work involving corrections to vibrational energy levels and anharmonic infrared and Raman spectra before and around 2016\cite{cornaton2016analytic}.  In the work with my (Dundas') PhD work it became necessary to use and automatize and these scripts, and I therefore decided to sew it all together to one package and at the same time author more necesary functionality.  As these scripts are a natural companion to enable applications of data produced with other software in our community (FraME/OpenRSP/LSDalton), it was also important that they were made readily available and more user friendly.  


\bibliographystyle{unsrt}
\bibliography{refs.bib}
	
\end{document}
